Track B: Minimum Fill-In
====

The code will be submitted to [Track 2 of PACE 2018](https://pacechallenge.wordpress.com/pace-2018/).

## Requirement
Java 1.8 or higher

## Build
Run build.sh.
```
./build.sh
```

## Usage
Run run.sh. The input graph is given from the standard input.
```
./run.sh < instance001.gr
```
Please see [here](https://pacechallenge.wordpress.com/pace-2018/) for the input graph format.

## Authors
Yasuaki Kobayashi (Kyoto University)