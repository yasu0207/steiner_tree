package tw.exact;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Set;
import java.util.Stack;

import graph.WeightedGraph;
import tw.exact.NiceTreeDecomposition.Type;

import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

public class NiceTreeDecomposition {
	private WeightedGraph graph;
	public Node rootNode;
	private XBitSet root;
	private int width;
	static final boolean DEBUG = true;
	static final boolean VERBOSE = false;

	public NiceTreeDecomposition(WeightedGraph graph, TreeDecomposition td){
		this.graph = graph;
		niceTreeDecomposition(td);
//		constructNiceTreeDecomposition(td);
		canonicalize();
		if (rootNode.vertex() == -1) {
			rootNode = rootNode.left;
		}
		
		setSubgraph(rootNode);
	}

	private void setSubgraph(Node node) {
		if (node.type == Type.leaf) {
			node.decendantBags = new XBitSet();
		} else if (node.type == Type.forget) {
			setSubgraph(node.left);
			node.decendantBags = node.left.decendantBags;
		} else if (node.type == Type.introduce_vertex) {
			setSubgraph(node.left);
			node.decendantBags = (XBitSet)node.left.decendantBags.clone();
			node.decendantBags.set(node.vertex());
		} else if (node.type == Type.join) {
			setSubgraph(node.left);
			setSubgraph(node.right);
			node.decendantBags = node.left.decendantBags.unionWith(node.right.decendantBags);
		}
	}

	private void niceTreeDecomposition(TreeDecomposition td){
		
		int terminal = graph.terms[0];
		// make a leaf containing a terminal
		for(int t = 1; t <= td.nb; t++) {
			if (td.bagSets[t].get(terminal)) {
				td.addBag(new int[] {terminal});
				td.addEdge(t, td.nb);
				td.addEdge(td.nb, t);
				break;
			}
		}
		
		for (int t = 1; t <= td.nb; t++) {
			Arrays.sort(td.bags[t]);
		}
		
//		td.validate();
		root = td.bagSets[td.nb];
		rootNode = new Node(root);
		dfs(td.nb, -1, rootNode, td);
	}

	private void insertFIPath(Node parent, Node child){
		XBitSet forgotten = child.bagSet.subtract(parent.bagSet);
		XBitSet introduced = parent.bagSet.subtract(child.bagSet);

		XBitSet base = (XBitSet)child.bagSet.clone();
		Node node = child;
		for(int v = forgotten.nextSetBit(0); v >= 0; v = forgotten.nextSetBit(v + 1)){
			base.clear(v);
			if(!base.equals(parent.bagSet)){
				node = new Node(Type.forget, (XBitSet)base.clone(), node);
			}
		}
		for(int v = introduced.nextSetBit(0); v >= 0; v = introduced.nextSetBit(v + 1)){
			base.set(v);
			if(!base.equals(parent.bagSet)){
				node = new Node(Type.introduce_vertex, (XBitSet)base.clone(), node);
			}
		}
		assert base.equals(parent.bagSet);

		parent.left = node;
		node.parent = parent;
		if(introduced.isEmpty()){
			parent.type = Type.forget;
		}
		else{
			parent.type = Type.introduce_vertex;
		}
	}

	private void dfs(int t, int r, Node node, TreeDecomposition td) {
		int degree = ((r != -1) ? td.degree[t] - 1 : td.degree[t]);

		if(degree <= 0){
			Node child = new Node();
			insertFIPath(node, child);
			child.type = Type.leaf;
			return;
		}

		if(degree == 1){
			int ch = ((td.neighbor[t][0] != r) ? td.neighbor[t][0] : td.neighbor[t][1]);
			Node child = new Node(td.bags[ch]);
			insertFIPath(node, child);
			dfs(ch, t, child, td);
			return;
		}

		Node[] parents = new Node[degree];
		int sp = 0;
		for(int i = 0; i < td.degree[t]; i++){
			int ch = td.neighbor[t][i];
			if(ch == r) continue;
			parents[sp] = new Node(node.bagSet);
			Node child = new Node(td.bags[ch]);
			insertFIPath(parents[sp], child);
			dfs(ch, t, child, td);
			++sp;
		}
		binaryTree(node, parents);
	}

	private void binaryTree(Node parent, Node[] children){
		assert children.length >= 2;

		Stack< Node > stack = new Stack< >();
		for(Node node: children){
			stack.push(node);
		}

		while(stack.size() != 2){
			Node node0 = stack.pop();
			Node node1 = stack.pop();
			stack.push(new Node(Type.join, parent.bagSet, node0, node1));
		}
		parent.left = stack.pop();
		parent.right = stack.pop();
		parent.left.parent = parent;
		parent.right.parent = parent;
		parent.type = Type.join;

		return;
	}

	public enum Type {
		forget, introduce_vertex, introduce_edge, join, leaf
	}
	public class Node{
		public Type type;
		public int[] bag;
		public XBitSet bagSet;
		public XBitSet decendantBags;
		public Node left, right;
		public Node parent;
		public int[] edge;

		private Node(Type type,
				int[] bag, XBitSet bagSet, Node left, Node right){
			this.type = type;
			this.bag = bag;
			this.bagSet = bagSet;
			this.left = left;
			this.right = right;
			assert Arrays.equals(bag, bagSet.toArray());
		}

		private Node(){
			this(null, new int[0], new XBitSet(), null, null);
		}

		private Node(Type type){
			this(type, new int[0], new XBitSet(), null, null);
		}

		private Node(XBitSet bagSet){
			this(null, bagSet.toArray(), bagSet, null, null);
		}

		private Node(int[] bag){
			this(null, bag, new XBitSet(bag), null, null);
		}

		private Node(Type type, XBitSet bagSet){
			this(type, bagSet.toArray(), bagSet, null, null);
		}

		private Node(Type type, int[] bag){
			this(type, bag, new XBitSet(bag), null, null);
		}

		private Node(int[] edge, int[] bag)
		{
			this(Type.introduce_edge, bag, new XBitSet(bag), null, null);
			this.edge = edge;
		}

		public Node(Type type, XBitSet bagSet, Node node) {
			this(type, bagSet.toArray(), bagSet, node, null);
		}

		public Node(Type type, XBitSet bagSet, Node left, Node right) {
			this(type, bagSet.toArray(), bagSet, left, right);
		}

		public int vertex() {
			switch(type){
			case forget:
				return left.bagSet.subtract(bagSet).nextSetBit(0);
			case introduce_vertex:
				return bagSet.subtract(left.bagSet).nextSetBit(0);
			default:
				break;
			}
			return -1;
		}

		public int indexOf(int v)
		{
			for (int i = 0; i < bag.length; i++) {
				if (bag[i] == v) {
					return i;
				}
			}
			return -1;
		}

		@Override
		public String toString(){
			String res = "[" + type + ":" + bagSet.toString();
			if (type == Type.introduce_edge) {
				res += " e" + Arrays.toString(edge);
			} else if (type == Type.forget || type == Type.introduce_vertex) {
				res += " v(" + vertex() + ")";
			}
			return res + "]";
		}

		private void print(String pref)
		{
			System.out.println(pref + this);
			if (left != null) {
				left.print(pref + "-");
			}
			if (right != null) {
				right.print(pref + "-");
			}
		}
	}
	
	public int size()
	{
		return size(rootNode);
	}

	private int size(Node node)
	{
		return node == null ? 0 : size(node.left) + size(node.right) + 1;
	}

	private void extendIntroEdgePath(Node parent, Node anchor)
	{ 
		if (parent.type != Type.forget) {
			return;
		}
		Node child = parent.left;
		XBitSet B = (XBitSet)parent.bagSet.clone();
		int u = parent.vertex();
		B.and(graph.neighborSet[u]);
		Node current = parent;
		for (int v = B.nextSetBit(0); v >= 0; v = B.nextSetBit(v + 1)) {
			current.left = new Node(new int[]{u, v}, anchor.bag);
			current.left.parent = current;
			current = current.left;
		}
		current.left = child;
		current.left.parent = current;
	}


	private void canonicalize()
	{
		reduceRedundant(rootNode, rootNode.left);
		canonicalize(rootNode);
	}

	private void reduceRedundant(Node parent, Node child) {
		if (child == null) return;
		if (child.type == Type.introduce_vertex) {
			reduceRedundant(child, child.left);
			if (child.vertex() == -1) {
				if (parent.left == child) {
					parent.left = child.left;
				} else {
					parent.right = child.left;
				}
			}
		} else if (child.type == Type.forget) {
			reduceRedundant(child, child.left);
			if (child.vertex() == -1) {
				if (parent.left == child) {
					parent.left = child.left;
				} else {
					parent.right = child.left;
				}
			}
		} else if (child.type == Type.introduce_edge) {
			reduceRedundant(child, child.left);
		} else if (child.type == Type.join) {
			reduceRedundant(child, child.left);
			reduceRedundant(child, child.right);
		}
	}

	private void canonicalize(Node node) 
	{
//		extendIntroEdgePath(node, node.left);
		if (node.left != null) {
			canonicalize(node.left);
		}
		if (node.right != null) {
			canonicalize(node.right);
		}
	}

//	private void validate(){
//		TreeDecomposition td = toTreeDecomposition();
//		td.writeTo(System.out);
//		assert root.bagSet.isEmpty();
//		td.validate();
//		isNice(rootNode);
//	}

	public boolean isNice(Node node){
		if(node == null){
			return true;
		}
		switch(node.type){
		case forget:
			if (node.left == null) {
				return false;
			}
			if (node.right != null) {
				return false;
			}
			if (node.left.bagSet.subtract(node.bagSet).cardinality() != 1) {
				return false;
			}
			break;
		case introduce_vertex:
			if (node.left == null) {
				return false;
			}
			if (node.right != null) {
				return false;
			}
			if (node.bagSet.subtract(node.left.bagSet).cardinality() != 1) {
				return false;
			}
			break;
		case introduce_edge:
			if (node.left == null) {
				return false;
			}
			if (node.right != null) {
				return false;
			}
			if (node.left.bagSet.subtract(node.bagSet).cardinality() != 0) {
				return false;
			}
			if (node.edge == null) {
				return false;
			}
			if (node.bagSet.get(node.edge[0]) == false || 
					node.bagSet.get(node.edge[1]) == false ||
					graph.areAdjacent(node.edge[0], node.edge[1]) == false) {
				return false;
			}
			break;
		case join:
			if (node.left == null) {
				return false;
			}
			if (node.right == null) {
				return false;
			}
			if (node.bagSet.equals(node.left.bagSet) == false) {
				return false;
			}
			if (node.bagSet.equals(node.right.bagSet) == false) {
				return false;
			}
			break;
		case leaf:
			if (node.left != null) {
				return false;
			}
			if (node.right != null) {
				return false;
			}
			if (node.bagSet.isEmpty() == false) {
				return false;
			}
			break;
		}
		if(isNice(node.left) == false){
			return false;
		}
		if(isNice(node.right) == false){
			return false;
		}
		return true;
	}

//	public TreeDecomposition toTreeDecomposition(){
//		if(root == null){
//			return null;
//		}
//		TreeDecomposition result = new TreeDecomposition(0, width, graph);
//		result.addBag(root.bag);
//		dfsToTreeDecomposition(root, 1, result);
//		return result;
//	}

//	private void dfsToTreeDecomposition(Node node, int t, TreeDecomposition result){
//		if(node == null){
//			return;
//		}
//		if(node.left != null){
//			result.addBag(node.left.bag);
//			result.addEdge(t, result.nb);
//			dfsToTreeDecomposition(node.left, result.nb, result);
//		}
//		if(node.right != null){
//			result.addBag(node.right.bag);
//			result.addEdge(t, result.nb);
//			dfsToTreeDecomposition(node.right, result.nb, result);
//		}
//	}

	public void printDecomposition() {
		rootNode.print("");
	}

}
