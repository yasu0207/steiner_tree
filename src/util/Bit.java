package util;

public class Bit {
	
	public static final boolean contains(int S, int k)
	{
		return ((S >> k) & 1) > 0;
	}
	
	public static final boolean intersects(int S, int T)
	{
		return (S & T) > 0;
	}
	
	// S \subseteq T
	public static final boolean isSubset(int S, int T)
	{
		return (S | T) == T;
	}
	
	public static final int insertZero(int S, int k)
	{
		int low = S & ((1 << k) - 1);
		return ((S ^ low) << 1) | low;
	}
	
	public static final int insertOne(int S, int k)
	{
		return insertZero(S, k) | (1 << k);
	}
	
	public static final int remove(int S, int k)
	{
		int low = S & ((1 << k) - 1);
		return (((S ^ low) & ~(1 << k)) >> 1) | low;
	}
	
	public static final int index(int S, int k) {
		return Integer.bitCount(S & ((1 << k) - 1));
	}
	
}