package util;

import java.util.Arrays;

import tw.exact.NiceTreeDecomposition.Node;

public class Partition implements Comparable<Partition>{

//	private static final boolean DEBUG = true;
	private static final boolean DEBUG = false;
	
	int[] sets;
	Partition prevL;
	Partition prevR;
	int prevS;
	int E;
	Node prevNodeL;
	Node prevNodeR;
	
	private Partition() {
		sets = new int[0]; //empty partition
	}
	
	public void setPrevL(Partition prevL)
	{
		this.prevL = prevL;
	}
	
	public void setPrevR(Partition prevR)
	{
		this.prevR = prevR;
	}
	
	public void setPrevS(int prevS)
	{
		this.prevS = prevS;
	}
	
	public void setE(int E)
	{
		this.E = E;
	}
	
	public void setPrevNodeL(Node prevNodeL)
	{
		this.prevNodeL = prevNodeL;
	}
	
	public void setPrevNodeR(Node prevNodeR)
	{
		this.prevNodeR = prevNodeR;
	}
	
	public Partition getPrevL()
	{
		return prevL;
	}
	
	public Partition getPrevR()
	{
		return prevR;
	}
	
	public int getPrevS()
	{
		return prevS;
	}
	
	public int getE()
	{
		return E;
	}
	
	public Node getPrevNodeL()
	{
		return prevNodeL;
	}
	
	public Node getPrevNodeR()
	{
		return prevNodeR;
	}
	
	private Partition(int[] sets) {
		this.sets = sets;
	}
	
	public static Partition emptyPartition()
	{
		return new Partition();
	}
	
	public Partition copy()
	{
		return new Partition(Arrays.copyOf(sets, sets.length));
	}
	
	public Partition merge(int pos1, int pos2) {
		int id1 = setIndex(pos1);
		int id2 = setIndex(pos2);
		if (id1 == id2) {
			return this;
		}
		Partition partition = new Partition(new int[sets.length - 1]);
		for (int i = 0, k = 0; i < sets.length; i++) {
			if (i != id1 && i != id2) {
				partition.sets[k++] = sets[i];
			}
		}
		partition.sets[partition.sets.length - 1] = sets[id1] | sets[id2];
		partition.canonicalize();
		
		if (DEBUG) {
			System.out.println("MERGE: " + this + " " + partition);
		}
		return partition;
	}

	public Partition insert(int pos) {
		Partition partition = new Partition(Arrays.copyOf(sets, sets.length + 1));
		for (int i = 0; i < sets.length; i++) {
			partition.sets[i] = Bit.insertZero(sets[i], pos);
		}
		partition.sets[partition.sets.length - 1] = 1 << pos;
		partition.canonicalize();
		
		if (DEBUG) {
			System.out.println("INSERT: " + pos + " " + this + " " + partition);
		}
		return partition;
	}

	public Partition remove(int pos) {
		int id = setIndex(pos);
		if (id < 0) {
			return this;
		}
		Partition partition = null;
		if (sets[id] == 1 << pos) {
			// pos is a singleton in this partition
			partition = new Partition(new int[sets.length - 1]);
			for (int i = 0, k = 0; i < sets.length; i++) {
				if (i != id) {
					partition.sets[k++] = Bit.remove(sets[i], pos);
				}
			}
		} else {
			partition = new Partition(Arrays.copyOf(sets, sets.length));
			for (int i = 0; i < sets.length; i++) {
				partition.sets[i] = Bit.remove(sets[i], pos);
			}
			partition.canonicalize();
		}
		if (DEBUG) {
			System.out.println("REMOVE: " + this + " " + partition);
		}
		return partition;
	}

	public Partition join(Partition partition)
	{
		Partition part = (Partition) partition;
		int[] tree = new int[sets.length + part.sets.length];
		Arrays.fill(tree, -1);
		for (int i = 0; i < sets.length; i++) {
			for (int j = 0; j < part.sets.length; j++) {
				if ((sets[i] & part.sets[j]) != 0) {
					union(tree, i, sets.length + j);
				}
			}
		}
		int roots = 0;
		int[] nsets = new int[sets.length];
		for (int i = 0; i < sets.length; i++) {
			if (tree[i] < 0) {
				tree[i] = (-roots - 1);
				nsets[roots++] |= sets[i];
			}
		}
		for (int i = 0; i < part.sets.length; i++) {
			if (tree[sets.length + i] < 0) {
				tree[sets.length + i] = (-roots - 1);
				nsets[roots++] |= part.sets[i];
			}
		}
		for (int i = 0; i < sets.length; i++) {
			if (tree[i] >= 0) {
				int r = root(tree, i);
				nsets[-tree[r] - 1] |= sets[i];
			}
		}
		for (int i = 0; i < part.sets.length; i++) {
			if (tree[sets.length + i] >= 0) {
				int r = root(tree, sets.length + i);
				nsets[-tree[r] - 1] |= part.sets[i];
			}
		}
		Partition newPartition = new Partition(Arrays.copyOf(nsets, roots));
		newPartition.canonicalize();
		
		if (DEBUG) {
			System.out.println("JOIN: " + this + " " + partition + " " + newPartition);
		}
		return newPartition;
	}

	public boolean isSingleton(int pos) {
		int id = setIndex(pos);
		return id >= 0 && sets[id] == 1 << pos;
	}
	
	public static Partition leastElement(int n)
	{
		int[] sets = new int[n];
		for (int i = 0; i < n; i++) {
			sets[i] = 1 << i;
		}
		return new Partition(sets);
	}

	public boolean equals(Object obj) {
		Partition partition = (Partition)obj;
		return Arrays.equals(sets, partition.sets);
	}

	public int hashCode() {
		return Arrays.hashCode(sets);
	}
	
	@Override
	public String toString() {
		if (sets.length == 0) return "{}";
		String res = "{";
		for (int set: sets) {
			String tmp = "";
			for (int i = 0; set > 0 && i < 32; set >>= 1, i++) {
				if ((set & 1) == 1) {
					tmp += i + ",";
				}
			}
			res += "[" + tmp.substring(0, tmp.length() - 1) + "]";
		}
		res = res + "}";
		return res;
	}
	
	private void canonicalize()
	{
		Arrays.sort(sets);
	}
	
	private int setIndex(int pos)
	{
		for (int i = 0; i < sets.length; i++) {
			if (Bit.contains(sets[i], pos)) {
				return i;
			}
		}
		return -1;
	}
	
	private final void union(int[] tree, int x, int y)
	{
		x = root(tree, x);
		y = root(tree, y);
		if(x != y) {
			if(tree[x] < tree[y]) {
				x ^= y; y ^= x; x^= y;
			}
			tree[x] += tree[y];
			tree[y] = x;
		}
	}
	private int root(int[] tree, int x)
	{
		return tree[x] < 0 ? x : (tree[x] = root(tree, tree[x]));
	}

	public boolean isRefinement(int S, int T)
	{
		for (int C: sets) {
			if (Bit.isSubset(C, S) == false && Bit.isSubset(C, T) == false) {
				return false;
			}
		}
		return true;
	}

	public Partition merge(int S)
	{
		int ps = 0;
		int SS = 0;
		int newSize = sets.length + 1;
		for (int i = 0; i < sets.length; i++) {
			if (Bit.intersects(S, sets[i])) {
				ps |= 1 << i;
				if (Bit.intersects(SS, sets[i]) == false) {
					newSize--;
					SS |= sets[i];
				}
			}
		}
		Partition partition = new Partition(new int[newSize]);
		for (int i = 0, j = 0; i < sets.length; i++) {
			if (Bit.contains(ps, i)) {
				partition.sets[newSize - 1] |= sets[i];
			} else {
				partition.sets[j++] = sets[i];
			}
		}
		partition.canonicalize();
		return partition;
	}
	
	public int blocks()
	{
		return sets.length;
	}
	
	public boolean isExtendable(Partition partition)
	{
//		return true;
		if (join(partition).blocks() <= 1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int compareTo(Partition partition) {
		if (sets.length != partition.sets.length) {
			return Integer.compare(sets.length, partition.sets.length);
		}
		for (int i = 0; i < sets.length; i++) {
			if (sets[i] != partition.sets[i]) {
				return Integer.compare(sets[i], partition.sets[i]);
			}
		}
		return 0;
	}
}
