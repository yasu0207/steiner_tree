package graph;

import java.util.Iterator;

public class Graph {
	
	public int n;
	public int m;
	public int t;
	
	public Vertex[] vs;
	public int[] terms;
	
	public Graph(int n, int m)
	{
		this.n = n;
		this.m = m;
		
		vs = new Vertex[n];
		for (int i = 0; i < n; i++) {
			vs[i] = new Vertex(i);
		}
	}
	
	public void addEdge(int u, int v, int w)
	{
		Edge e = new Edge(vs[u], vs[v], w);
		vs[u].inc.add(e);
		vs[v].inc.add(e);
	}
	
	@Override
	public String toString() {
		String res = "";
		for (Vertex v: vs) {
			for (Edge e: v) {
				if (e.u == v) {
					res += e.u.id + " " + e.v.id + " " + e.w + "\n";
				}
			}
		}
		
		for (int i = 0; i < t; i++) {
			res += terms[i] + "\n";
		}
		return res;
	}

	public void remove(Vertex v)
	{
		
	}

}
