package graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

import tw.exact.Graph;
import tw.exact.TreeDecomposition;
import tw.exact.XBitSet;

public class WeightedGraph extends Graph {

	private static final boolean DEBUG = true;
//	private static final boolean DEBUG = false;
	
	private static final int inf = 1 << 25;
	private Map<Integer, Integer>[] weight;
	
	public int t;
	public int[] terms;
	private boolean[] isTerminal;
	
	public int[] label;
	
	public WeightedGraph(int n) {
		super(n);
		t = 0;
		weight = new Map[n];
		terms = new int[0];
		isTerminal = new boolean[n];
		for (int i = 0; i < n; i++) {
			weight[i] = new HashMap<>();
		}
		
		label = new int[n];
		for (int i = 0; i < n; i++) {
			label[i] = i + 1;
		}
	}
	
	
	public boolean addTerminal(int v) {
		if (isTerminal(v)) {
			return false;
		}
		terms = Arrays.copyOf(terms, t + 1);
		terms[t] = v;
		isTerminal[v] = true;
		t++;
		return true;
	}
	
	public void addEdge(int u, int v, int w)
	{
		Integer old = weight[Math.min(u, v)].get(Math.max(u, v));
		if (old == null || old > w) {
			weight[Math.min(u, v)].put(Math.max(u, v), w);
		}
		addEdge(u, v);
	}
	
	public int getEdgeWeight(int u, int v)
	{
		Integer val = weight[Math.min(u, v)].get(Math.max(u, v));
		return val == null ? 0 : val;
	}
	
	public boolean isTerminal(int v)
	{
		return isTerminal[v];
	}
	
	private int[] num;
	private int[] low;
	private int time;
	private ArrayList<XBitSet> components;
	Stack<Integer> st;
	
	public ArrayList<XBitSet> decomposeByCutPoints()
	{
		components = new ArrayList< >();
		num = new int[ n ];
		low = new int[ n ];
		st = new Stack<>();
		
		for (int u = 0; u < n; u++) {
			if (num[ u ] == 0) {
				time = 0;
				visitForBiComponent( -1, u );
			}
		}
		
		return components;
	}
	
	private void visitForBiComponent(int p, int u)
	{
		low[ u ] = num[ u ] = ++time;
		st.push( u );
		for (int v: neighbor[ u ]) {
			if (num[ v ] == 0) {
				visitForBiComponent( u, v );
				low[ u ] = Math.min( low[ u ], low[ v ] );
				if (low[ v ] >= num[ u ]) {
					XBitSet bcomp = new XBitSet( n );
					bcomp.set( u );
					while (true) {
						int w = st.pop();
						bcomp.set( w );
						if (w == v) {
							break;
						}
					}
					components.add( bcomp );
				}
			} else {
				low[ u ] = Math.min( low[ u ], num[ v ] );
			}
		}
	}
	
	public static WeightedGraph randomGraph(int N, int M, int T, int ub)
	{
		return randomGraph(N, M, T, ub, new Random().nextLong());
	}
	
	public static WeightedGraph randomGraph(int N, int M, int T, int ub, long seed)
	{
		if (DEBUG) {
			System.out.println("seed: " + seed);
		}
		Random rand = new Random(seed);
		while (true) {
			WeightedGraph g = new WeightedGraph(N);
			for (int i = 0; i < M;) {
				int u = rand.nextInt(N);
				int v = rand.nextInt(N);

				if (u == v || g.areAdjacent(u, v)) {
					continue;
				}
				int w = rand.nextInt(ub) + 1;
				g.addEdge(u, v, w);
				i++;
			}
			if (g.isConnected() == false) {
				continue;
			}
			
			for (int i = 0; i < T;) {
				int t = rand.nextInt(N);
				if (g.addTerminal(t) == false) {
					continue;
				}
				i++;
			}
			return g;
		}
	}
	
	public static Object[] readGraphGrwithTd(Scanner sc)
	{
		
		WeightedGraph g = null;
		TreeDecomposition td = null;
		
		while (sc.hasNextLine()) {
			String[] tokens = sc.nextLine().split("\\s+");
			if (tokens.length <= 1) {
				continue;
			}
			
			if (tokens[1].equals("Graph")) {
				int n = Integer.parseInt(sc.nextLine().split("\\s+")[1]);
				int m = Integer.parseInt(sc.nextLine().split("\\s+")[1]);
				
				g = new WeightedGraph(n);
				for (int i = 0; i < m; i++) {
					tokens = sc.nextLine().split("\\s+");
					int u = Integer.parseInt(tokens[1]) - 1;
					int v = Integer.parseInt(tokens[2]) - 1;
					int w = Integer.parseInt(tokens[3]);
					g.addEdge(u, v, w);
				}
				
				sc.nextLine(); //END
			} else if (tokens[1].equals("Terminals")) {
				g.t = Integer.parseInt(sc.nextLine().split("\\s+")[1]);
				g.terms = new int[g.t];
				for (int i = 0; i < g.t; i++) {
					g.terms[i] = Integer.parseInt(sc.nextLine().split("\\s+")[1]) - 1;
					g.isTerminal[g.terms[i]] = true;
				}
				sc.nextLine(); //END
			} else if (tokens[1].equals("Tree")) {
				tokens = sc.nextLine().split("\\s+");
				if (tokens.length == 0 || tokens[0].startsWith("c")) {
					tokens = sc.nextLine().split("\\s+");
				}
				int nb = Integer.parseInt(tokens[2]);
				int width = Integer.parseInt(tokens[3]) - 1;
				
				td = new TreeDecomposition(nb, width, g);
				
				for (int i = 0; i < nb; i++) {
					tokens = sc.nextLine().split("\\s+");
					if (tokens.length == 0) continue;
					int bag[] = new int[tokens.length - 2];
					for (int j = 0; j < bag.length; j++) {
						bag[j] = Integer.parseInt(tokens[j + 2]) - 1;
					}
					td.setBag(Integer.parseInt(tokens[1]), bag);
				}
				for (int i = 0; i < nb - 1; i++) {
					tokens = sc.nextLine().split("\\s+");
					int j = Integer.parseInt(tokens[0]);
					int k = Integer.parseInt(tokens[1]);
					if (j == 0 || k == 0) continue;
					td.addEdge(j, k);
					td.addEdge(k, j);
				}
			}
		}
		
		return new Object[] {g, td};
	}
	
	public static Object[] readGraphGrwithTd(String filename)
	{
		return readGraphGrwithTd(new File(filename));
	}
	
	public static Object[] readGraphGrwithTd()
	{
		return readGraphGrwithTd(new Scanner(System.in));
	}
	
	public static Object[] readGraphGrwithTd(File file) {
		try {
			Scanner sc = new Scanner(file);
			return readGraphGrwithTd(sc);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static WeightedGraph readGraphGr()
	{
		return readGraphGr(new Scanner(System.in));
	}
	public static WeightedGraph readGraphGr(String filename)
	{
		return readGraphGr(new File(filename));
	}
	
	public static WeightedGraph readGraphGr(File file)
	{
		try {
			Scanner sc = new Scanner(file);
			return readGraphGr(sc);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static WeightedGraph readGraphGr(Scanner sc)
	{

		sc.next();//SECTION
		sc.next();//Graph
		sc.next();//Nodes
		int n = sc.nextInt();
		sc.next();//Edges
		int m = sc.nextInt();

		WeightedGraph g = new WeightedGraph(n);

		for (int i = 0; i < m; i++) {
			sc.next(); //E
			int u = sc.nextInt() - 1;
			int v = sc.nextInt() - 1;
			int w = sc.nextInt();
			g.addEdge(u, v, w);
		}

		sc.next(); //END
		sc.next(); //SECTION
		sc.next(); //Terminals
		sc.next(); //Terminals

		g.t = sc.nextInt();
		g.terms = new int[g.t];
		for (int i = 0; i < g.t; i++) {
			sc.next(); //T
			g.terms[i] = sc.nextInt() - 1;
			g.isTerminal[g.terms[i]] = true;
		}
		sc.next(); //END
		sc.next(); //EOF

		return g;
	}

	@Override
	public String toString()
	{
		String res = "SECTION Graph\n";
		res += "Nodes " + n + "\n";
		res += "Edges " + numberOfEdges() + "\n";
		
		for (int i = 0; i < n; i++) {
			for (int j = neighborSet[i].nextSetBit(i + 1); j >= 0; j = neighborSet[i].nextSetBit(j + 1)) {
				res += "E " + label[i] + " " + label[j] + " " + getEdgeWeight(i, j) +  "\n";
			}
		}
		res += "END\n";
		res += "\n";
		res += "SECTION Terminals\n";
		res += "Terminals " + t + "\n";
		for (int i = 0; i < t; i++) {
			res += "T " + (label[terms[i]]) + "\n";
		}
		res += "END\n";
		res += "EOF";
		return res;
	}
}
