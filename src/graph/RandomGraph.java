package graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class RandomGraph {
	
	public static Graph random(int N, int M, int T)
	{
		long seed = new Random().nextLong();
		System.out.println("seed: " + seed);
		return random(N, M, T, seed);
	}
	
	public static Graph random(int N, int M, int T, long seed)
	{
		Random random = new Random(seed);
		Graph g = new Graph(N, M);
		boolean[][] adj = new boolean[N][N];
		for (int i = 0; i < N - 1; i++) {
			int k = random.nextInt(i + 1);
			adj[k][i + 1] = adj[i + 1][k] = true;
			g.addEdge(k, i + 1, random.nextInt(100));
		}
		for (int i = N - 1; i < M; ) {
			int s = random.nextInt(N);
			int t = random.nextInt(N);
			if (s != t && adj[s][t] == false) {
				adj[s][t] = adj[t][s] = true;
				i++;
				g.addEdge(s, t, random.nextInt(100));
			}
		}
		g.t = T;
		g.terms = new int[T];
		ArrayList<Integer> idx = new ArrayList<>();
		for (int i = 0; i < N; i++) {
			idx.add(i);
		}
		Collections.shuffle(idx, random);
		for (int i = 0; i < T; i++) {
			g.terms[i] = idx.get(i);
		}
		
		System.out.println(g);
		
		return g;
	}
	
	
}
