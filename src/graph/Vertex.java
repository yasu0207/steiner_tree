package graph;

import java.util.ArrayList;
import java.util.Iterator;

public class Vertex implements Iterable<Edge> {
	public int id;
	ArrayList<Edge> inc;
	
	public Vertex(int id)
	{
		this.id = id;
		inc = new ArrayList<>();
	}

	@Override
	public Iterator<Edge> iterator() {
		return inc.iterator();
	}
	
	public int degree()
	{
		return inc.size();
	}

}
