package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Reader {

	public static Graph read(String fileName)
	{
		try {
			return read(new Scanner(new File(fileName)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static Graph read()
	{
		return read(new Scanner(System.in));
	}
	
	private static Graph read(Scanner sc)
	{
		sc.next();//SECTION
		sc.next();//Graph
		sc.next();//Nodes
		int n = sc.nextInt();
		sc.next();//Edges
		int m = sc.nextInt();
		
		Graph g = new Graph(n, m);
		
		for (int i = 0; i < m; i++) {
			sc.next(); //E
			int u = sc.nextInt() - 1;
			int v = sc.nextInt() - 1;
			int w = sc.nextInt();
			g.addEdge(u, v, w);
		}
		
		sc.next(); //END
		sc.next(); //SECTION
		sc.next(); //Terminals
		sc.next(); //Terminals
		
		g.t = sc.nextInt();
		g.terms = new int[g.t];
		for (int i = 0; i < g.t; i++) {
			sc.next(); //T
			g.terms[i] = sc.nextInt() - 1;
		}
		sc.next(); //END
		sc.next(); //EOF
		
		return g;
	}
}
