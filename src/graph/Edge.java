package graph;

public class Edge {
	
	Vertex u;
	Vertex v;
	public int w;
	
	public Edge(Vertex u, Vertex v, int w)
	{
		this.u = u;
		this.v = v;
		this.w = w;
	}
	
	public Vertex opposite(Vertex x)
	{
		return x == u ? v : u;
	}
}
