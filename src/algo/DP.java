package algo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.TreeSet;

import graph.Edge;
import graph.Graph;
import graph.RandomGraph;
import graph.Reader;
import graph.Vertex;
import graph.WeightedGraph;
import tw.exact.TreeDecomposition;
import util.UnionFind;
import util.Pair;

public class DP {

	
	WeightedGraph g;
	int[][] path;
	int[][] prev;
	
	int[][] dp;
	
	private static final int inf = 1 << 25;
	
	public DP(WeightedGraph g)
	{
		this.g = g;
	}
	
	public Pair<Integer, TreeSet<int[]>> solve()
	{
		if (g.t < 2) {
			return new Pair(0, new TreeSet());
		}
		
		shortestPaths();
		
		dp = new int[1 << g.t][g.n];
		
		for (int X = 0; X < 1 << g.t; X++) {
			Arrays.fill(dp[X], inf);
		}
		for (int k = 0; k < g.t; k++) {
			for (int v = 0; v < g.n; v++) {
				dp[1 << k][v] = path[g.terms[k]][v];
			}
		}
		for (int X = 1; X < 1 << g.t; X++) {
			if (((X - 1) & X) == 0) continue;
			for (int Y = (X - 1) & X; Y > 0; Y = (Y - 1) & X) {
				if (Integer.bitCount(X) > Integer.bitCount(X)) continue;
				for (int v = 0; v < g.n; v++) {
					dp[X][v] = Math.min(dp[X][v], dp[Y][v] + dp[X ^ Y][v]);
				}
			}
			for (int u = 0; u < g.n; u++) {
				for (int v = 0; v < g.n; v++) {
					dp[X][u] = Math.min(dp[X][u], dp[X][v] + path[u][v]);
				}
			}
		}
		
		int opt = inf;
		int optY = -1;
		int optw = -1;
		for (int X = 0; X < 1 << g.t; X++) {
			for (int v = 0; v < g.n; v++) {
				if (opt > dp[X][v] + dp[(1 << g.t) - 1 - X][v]) {
					opt = dp[X][v] + dp[(1 << g.t) - 1 - X][v];
					optY = X;
					optw = v;
				}
			}
		}
		
		HashSet<int[]> edges = new HashSet<>();
		constructTree((1 << g.t) - 1 - optY, optw, prev, edges);
		constructTree(optY, optw, prev, edges);
		return new Pair(opt, edges);
	}

	private void shortestPaths()
	{
		path = new int[g.n][g.n];
		prev  = new int[g.n][g.n];
		for (int i = 0; i < g.n; i++) {
			Arrays.fill(path[i], inf);
			Arrays.fill(prev[i], -1);
		}

		for (int s = 0; s < g.n; s++) {
			PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> a[1] - b[1]);
			path[s][s] = 0;
			pq.add(new int[]{s, 0});
			while (pq.isEmpty() == false) {
				int u = pq.peek()[0];
				int d = pq.poll()[1];
				if (path[s][u] < d) continue;
				for (int v: g.neighbor[u]) {
					int w = g.getEdgeWeight(u, v);
					if (path[s][v] > d + w) {
						path[s][v] = d + w;
						prev[s][v] = u;
						pq.add(new int[]{v, path[s][v]});
					}
				}
			}
		}
		
	}
	
	private void constructTree(int X, int v, int[][] prev, HashSet<int[]> edges)
	{
		if (Integer.bitCount(X) == 1) {
			for (int u = 0; u < g.n; u++) {
				if ((X >> u) == 1) {
					constructPath(g.terms[u], v, prev, edges); 
					return;
				}
			}
		} else {
			int optY = -1;
			for (int Y = (X - 1) & X; Y > 0; Y = (Y - 1) & X) {
				if (dp[X][v] == dp[Y][v] + dp[X ^ Y][v]) {
					optY = Y;
					break;
				}
			}
			if (optY != -1) {
				constructTree(optY, v, prev, edges);
				constructTree(X ^ optY, v, prev, edges);
			} else {
				int optu = -1;
				for (int u = 0; u < g.n; u++) {
					if (u == v) continue;
					if (dp[X][v] == dp[X][u] + path[u][v]) {
						optu = u;
						break;
					}
				}
				if (optu >= 0) {
					constructTree(X, optu, prev, edges);
					constructPath(optu, v, prev, edges);
				}
			}
		}
	}
	
	private void constructPath(int u, int v, int[][] prev, HashSet<int[]> edges)
	{
		while (prev[u][v] != -1) {
			edges.add(new int[]{g.label[prev[u][v]], g.label[v]});
			v = prev[u][v];
		}
	}
	
}
