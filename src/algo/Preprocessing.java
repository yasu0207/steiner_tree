package algo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import graph.WeightedGraph;
import tw.exact.TreeDecomposition;
import tw.exact.XBitSet;
import util.Pair;

public class Preprocessing {
	
	HashMap<int[], int[]> chains;
	
	public Preprocessing()
	{
		chains = new HashMap<>();
	}
	
	public Pair<WeightedGraph, TreeDecomposition> contractMaximalChains(WeightedGraph g, TreeDecomposition td)
	{
		XBitSet deg2 = new XBitSet();
		for (int v = 0; v < g.n; v++) {
			if (g.degree[v] == 2 && g.isTerminal(v) == false) {
				deg2.set(v);
			}
		}
		
		if (deg2.isEmpty()) {
			return new Pair(g, td);
		}
		
		XBitSet remove = (XBitSet)deg2.clone();
		while (deg2.isEmpty() == false) {
			int v = deg2.nextSetBit(0);
			XBitSet path = new XBitSet();
			path.set(v);
			LinkedList<Integer> vertices = new LinkedList<>();
			vertices.add(v);
			int s = g.neighbor[v][0];
			while (g.degree[s] == 2)  {
				if (g.isTerminal(s)) {
					break;
				}
				path.set(s);
				vertices.addFirst(s);
				s = path.get(g.neighbor[s][0]) ? g.neighbor[s][1] : g.neighbor[s][0]; 
			}
			int t = g.neighbor[v][1];
			while (g.degree[t] == 2)  {
				if (g.isTerminal(t)) {
					break;
				}
				path.set(t);
				vertices.addLast(t);
				t = path.get(g.neighbor[t][0]) ? g.neighbor[t][1] : g.neighbor[t][0]; 
			}
			deg2.andNot(path);
			vertices.addFirst(s);
			vertices.addLast(t);
			int weight = 0;
			int prev = -1;
			for (int w: vertices) {
				if (prev >= 0) {
					weight += g.getEdgeWeight(prev, w);
				}
				prev = w;
			}
			// s and t are the ends of the maximal path that contains no terminal nodes
			if (g.areAdjacent(s, t) == false || g.getEdgeWeight(s, t) > weight) {
				g.addEdge(s, t, weight);
				chains.put(new int[] {Math.min(g.label[s], g.label[t]), Math.max(g.label[s], g.label[t])},
						path.toArray());
			}
		}
		
		WeightedGraph h = inducedSubgraph(g, g.all.subtract(remove)).first;
		TreeDecomposition htd = inducedDecomposition(td, h, g.all.subtract(remove));
		return new Pair(h, htd);
	}
	
	public int[][] get(int[] edge) {
		Arrays.sort(edge);
		int[] path = chains.get(edge);
		if (path == null) {
			return new int[][] {edge};
		} else {
			int[][] res = new int[path.length - 1][];
			for (int i = 0; i < path.length; i++) {
				res[i] = new int[] {path[i], path[i + 1]};
			}
			return res;
		}
	}
	
	public ArrayList<Pair<WeightedGraph, TreeDecomposition>> reduce(WeightedGraph g, TreeDecomposition td)
	{
		ArrayList<Pair<WeightedGraph, TreeDecomposition>> res = new ArrayList<>();
		XBitSet terminals = new XBitSet();
		for (int i = 0; i < g.t; i++) {
			terminals.set(g.terms[i]);
		}
		XBitSet articulations = g.articulations(g.all);
		
		if (articulations.cardinality() == 0) {
			res.add(new Pair(g, td));
			return res;
		}
		
		ArrayList<XBitSet> comps = g.decomposeByCutPoints();
		Map<Integer, XBitSet> incidence = new HashMap<>();
		for (int v = articulations.nextSetBit(0); v >= 0; v = articulations.nextSetBit(v + 1)) {
			incidence.put(v, new XBitSet());
		}
		for (int i = 0; i < comps.size(); i++) {
			XBitSet arts = comps.get(i).intersectWith(articulations);
			for (int v = arts.nextSetBit(0); v >= 0; v = arts.nextSetBit(v + 1)) {
				incidence.get(v).set(i);
			}
		}
		
		Queue<Integer> queue = new LinkedList<>();
		for (int i = 0; i < comps.size(); i++) {
			if (comps.get(i).intersectWith(articulations).cardinality() == 1) {
				if (comps.get(i).isDisjoint(terminals)) {
					queue.add(i);
				}
			}
		}
		
		XBitSet remain = (XBitSet)articulations.clone();
		boolean[] remove = new boolean[comps.size()];
		while (queue.isEmpty() == false) {
			int k = queue.poll();
			remove[k] = true;
			int v = remain.intersectWith(comps.get(k)).nextSetBit(0);
			XBitSet inc = incidence.get(v);
			inc.clear(k);
			if (inc.cardinality() == 1) {
				int kk = inc.nextSetBit(0);
				remain.clear(v);
				if (comps.get(kk).isDisjoint(terminals)) {
					queue.add(inc.nextSetBit(0));
				}
			}
		}
		
		for (int i = 0; i < comps.size(); i++) {
			XBitSet newTerms = comps.get(i).intersectWith(remain);
			Pair<WeightedGraph, int[]> subg = inducedSubgraph(g, comps.get(i));
			WeightedGraph h = subg.first;
			int[] map = subg.second;
			for (int v = newTerms.nextSetBit(0); v >= 0; v = newTerms.nextSetBit(v + 1)) {
				h.addTerminal(map[v]);
			}
			if (h.t == 0) continue;
			TreeDecomposition htd = inducedDecomposition(td, h, comps.get(i));
			res.add(new Pair(h, htd));
		}
		
		
		return res;
	}

	public Pair<WeightedGraph, int[]> inducedSubgraph(WeightedGraph h, XBitSet S)
	{
		int in = S.cardinality();
		WeightedGraph ih = new WeightedGraph(in);
		int[] map = new int[h.n];
		for (int u = 0, v = S.nextSetBit(0); u < in; u++, v = S.nextSetBit(v + 1)) {
			ih.label[u] = h.label[v];
			map[v] = u;
			if (h.isTerminal(v)) {
				ih.addTerminal(u);
			}
		}
		for (int u = S.nextSetBit(0); u >= 0; u = S.nextSetBit(u + 1)) {
			XBitSet Nu = h.neighborSet[u].intersectWith(S);
			for (int v = Nu.nextSetBit(u + 1); v >= 0; v = Nu.nextSetBit(v + 1)) {
				ih.addEdge(map[u], map[v], h.getEdgeWeight(u, v));
			}
		}
		return new Pair<>(ih, map);
	}
	
	public TreeDecomposition inducedDecomposition(TreeDecomposition td, WeightedGraph h, XBitSet S)
	{
		if (S.cardinality() == 2) {
			TreeDecomposition itd = new TreeDecomposition(0, 1, h);
			itd.addBag(new int[] {0, 1});
			return itd;
		}
		int r = -1;
		for (int i = 1; i <= td.nb; i++) {
			if (td.bagSets[i].intersects(S)) {
				r = i;
				break;
			}
		}
		int[] parent = new int[td.nb + 1];
		Arrays.fill(parent, -1);
		dfs(td, r, -1, -1, parent, S);
		
		int[] map = new int[td.g.n];
		for (int u = 0, v = S.nextSetBit(0); u < h.n; u++, v = S.nextSetBit(v + 1)) {
			map[v] = u;
		}
		
		TreeDecomposition itd = new TreeDecomposition(0, 0, h);
		int width = 0;
		int[] bagmap = new int[td.nb + 1];
		for (int i = 1; i <= td.nb; i++) {
			XBitSet B = td.bagSets[i].intersectWith(S);
			if (i != r && parent[i] <= 0) continue;
			int[] bag = new int[B.cardinality()];
			for (int v = B.nextSetBit(0), k = 0; v >= 0; v = B.nextSetBit(v + 1), k++) {
				bag[k] = map[v];
			}
			itd.addBag(bag);
			bagmap[i] = itd.nb;
			width = Math.max(width, bag.length - 1);
		}
		for (int i = 1; i <= td.nb; i++) {
			if (parent[i] > 0) {
				itd.addEdge(bagmap[i], bagmap[parent[i]]);
			}
		}
		itd.width = width;
		return itd;
	}

	private void dfs(TreeDecomposition td, int r, int p, int q, int[] parent, XBitSet S) {
		boolean intersect = S.intersects(td.bagSets[r]);
		boolean same = q >= 0 && td.bagSets[q].intersectWith(S).equals(td.bagSets[r].intersectWith(S));
		if (intersect && same == false) {
			parent[r] = q;
		}
		for (int v: td.neighbor[r]) {
			if (v == p) continue;
			if (intersect && same == false) {
				dfs(td, v, r, r, parent, S);
			} else {
				dfs(td, v, r, q, parent, S);
			}
		}
	}
}
