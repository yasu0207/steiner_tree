package algo;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;

import graph.WeightedGraph;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TreeSet;

import tw.exact.NiceTreeDecomposition;
import tw.exact.NiceTreeDecomposition.Node;
import tw.exact.NiceTreeDecomposition.Type;
import util.Bit;
import util.Pair;
import util.Partition;
import util.UnionFind;
import tw.exact.TreeDecomposition;
import tw.exact.XBitSet;

public class TreewidthDP {

	WeightedGraph g;
	NiceTreeDecomposition td;
	XBitSet terminalSet;
	
	private int UB;
	private int completed;
	private int nNodes;
	
	private static final int inf = 1 << 25;

	
	public TreewidthDP(WeightedGraph g, NiceTreeDecomposition td)
	{
		this.g = g;
		this.td = td;
		terminalSet = new XBitSet(g.terms);
		UB = upperbound();
		
		if (VERBOSE) {
			System.out.println("upperbound = " + UB);
			completed = 0;
			nNodes = td.size();
		}
	}
	
	private int width(Node node) {
		int width = node.bag.length;
		if (node.type == Type.leaf) {
			return width;
		} else if (node.type == Type.forget) {
			return Math.max(width, width(node.left));
		} else if (node.type == Type.introduce_vertex) {
			return Math.max(width, width(node.left));
		} else if (node.type == Type.introduce_edge) {
			return Math.max(width, width(node.left));
		} else if (node.type == Type.forget) {
			return Math.max(width, Math.max(width(node.left), width(node.right)));
		}
		return 0;
	}

	private static void validate(WeightedGraph g, HashSet<int[]> edges, int opt)
	{
		HashSet<Integer> spanned = new HashSet<>();
		int weight = 0;
		for (int[] edge: edges) {
			if (g.areAdjacent(edge[0] - 1, edge[1] - 1) == false) {
				throw new RuntimeException("(" + edge[0] +  ", " + edge[1] + ") is not an edge");
			}
			spanned.add(edge[0] - 1);
			spanned.add(edge[1] - 1);
			weight += g.getEdgeWeight(edge[0] - 1, edge[1] - 1);
		}
		if (spanned.size() != edges.size() + 1) {
			throw new RuntimeException("the answer is not a tree");
		}
		for (int v: g.terms) {
			if (spanned.contains(v) == false) {
				throw new RuntimeException("the answer does not contain a terminal " + v);
			}
		}
		
		if (weight != opt) {
			throw new RuntimeException("the tree is not optimal");
		}
		
	}
	
	public int upperbound()
	{
		// compute a minimum spanning tree of g and remove all irrelevant vertices (non-terminal leaves)
		UnionFind uf = new UnionFind(g.n);
		int m = 0;
		for (int u = 0; u < g.n; u++) {
			m += g.degree[u];
		}
		m /= 2;
		int[][] edges = new int[m][];
		for (int u = 0, k = 0; u < g.n; u++) {
			for (int v = g.neighborSet[u].nextSetBit(u + 1); v >= 0; v = g.neighborSet[u].nextSetBit(v + 1)) {
				edges[k++] = new int[] {u, v, g.getEdgeWeight(u, v)};
			}
		}
		Arrays.sort(edges, (e, f) -> e[2] - f[2]);
		int sol = 0;
		TreeSet<Integer>[] inc = new TreeSet[g.n];
		for (int i = 0; i < g.n; i++) {
			inc[i] = new TreeSet<>();
		}
		for (int[] edge: edges) {
			if (uf.find(edge[0], edge[1]) == false) {
				sol += edge[2];
				uf.union(edge[0], edge[1]);
				inc[edge[0]].add(edge[1]);
				inc[edge[1]].add(edge[0]);
			}
		}
		Queue<Integer> que = new LinkedList<>();
		for (int i = 0; i < g.n; i++) {
			if (g.isTerminal(i) == false && inc[i].size() == 1) {
				que.add(i);
			}
		}
		while (que.isEmpty() == false) {
			int v = que.poll();
			if (inc[v].size() != 1) continue;
			int w = inc[v].first();
			inc[w].remove(v);
			if (g.isTerminal(w) == false && inc[w].size() == 1) {
				que.add(w);
			}
			sol -= g.getEdgeWeight(v, w);
		}
		return sol;
	}
	
	
	public Pair<Integer, HashSet<int[]>> solve()
	{
		if (pre_solve) {
			HashMap<Partition, Integer>[] sol = dfs(td.rootNode);
			if (sol[1] != null && sol[1].isEmpty() == false) {
				Integer value = sol[1].values().iterator().next();
				if (value != null) {
					if (VERBOSE) {
						System.out.println("UB: " + UB + "->" + value);
					}
					UB = Math.min(UB, value);
				}
			}
			pre_solve = false;
			completed = 0;
		}
		
		HashMap<Partition, Integer>[] sol = dfs(td.rootNode);
		Partition partition = sol[1].keySet().iterator().next();
		HashSet<int[]> edges = new HashSet<>();
		build(td.rootNode, 1, partition, edges);
		int ans = 0;
		for (int[] edge: edges) {
			ans += g.getEdgeWeight(edge[0], edge[1]);
		}
		HashSet<int[]> labels = new HashSet<>();
		for (int[] edge: edges) {
			labels.add(new int[] {g.label[edge[0]], g.label[edge[1]]});
		}
		if (VERBOSE) {
			for (int[] e: labels) {
				System.out.println(e[0] + " " + e[1]);
			}
		}
		return new Pair(ans, labels);
	}
	
	private void build(Node node, int S, Partition partition, HashSet<int[]> edges) {
		if (VERBOSE) {
			System.out.println(node + " " + Integer.toBinaryString(S) + " " + partition);
		}
		
		if (node.type == Type.leaf) {
			return;
		} else if (node.type == Type.introduce_vertex) {
			int v = node.vertex();
			int bid = node.indexOf(v);
			if (Bit.contains(S, bid)) {
				int TT = partition.getE();
				for (int i = 0; i < node.bag.length; i++) {
					if (Bit.contains(S, i)) {
						if ((TT & 1) > 0) {
							edges.add(new int[] {node.bag[i], v});
						}
						TT >>= 1;
					}
				}
				build(partition.getPrevNodeL(), partition.getPrevS(), partition.getPrevL(), edges);
			} else {
				build(partition.getPrevNodeL(), partition.getPrevS(), partition.getPrevL(), edges);
			}
		} else if (node.type == Type.forget) {
			build(partition.getPrevNodeL(), partition.getPrevS(), partition.getPrevL(), edges);
		} else if (node.type == Type.join) {
			build(partition.getPrevNodeL(), partition.getPrevS(), partition.getPrevL(), edges);
			build(partition.getPrevNodeR(), partition.getPrevS(), partition.getPrevR(), edges);
		}
	}
	
	
	private HashMap<Partition, Integer>[] dfs(Node node)
	{
		int w =  node.bag.length;
		
		HashMap<Partition, Integer>[] parent = new HashMap[1 << w];
		for (int S = 0; S < 1 << w; S++) {
			parent[S] = new HashMap<>(1 << Integer.bitCount(S));
		}
		
		int[][] btt = bagToTerminal(node);
		
		if (node.type == Type.leaf) {
			if (VERBOSE) {
				System.out.println("DP " + completed++ + "/" + nNodes + ", bagsize = " + w + ", node type = leaf");
			}
			parent[0] = new HashMap<>();
			parent[0].put(Partition.emptyPartition(), 0);	
		} else if (node.type == Type.introduce_vertex) {
			HashMap<Partition, Integer>[] ch = dfs(node.left);

			if (VERBOSE) {
				System.out.println("DP " + completed++ + "/" + nNodes + ", bagsize = " + w + ", node type = intro");
			}
			int v = node.vertex(); // introducing vertex;
			int bid = node.indexOf(v);
			int Nv = 0;
			for (int u: g.neighbor[v]) {
				int idx = node.indexOf(u);
				if (idx >= 0) {
					Nv |= 1 << idx;
				}
			}
			for (int S = 0; S < 1 << w; S++) {
				if (Bit.contains(S, bid)) {
					int worst = -1;
					int sid = Bit.index(S, bid);
					int U = S & Nv;
					int T = U;
					int LB = lowerbound(node.bag, S, btt);
							
					int pS = Bit.remove(S, bid);
					Entry<Partition, Integer>[] entries = ch[pS].entrySet().toArray(new Entry[0]); 
					Arrays.sort(entries, (a, b) -> a.getValue() - b.getValue());
					do {
						int TT = 0;
						int weight = 0;
						for (int i = 0, j = 0; i < w; i++) {
							if (Bit.contains(S, i)) {
								if (Bit.contains(T, i)) {
									TT |= 1 << j;
									weight += g.getEdgeWeight(node.bag[i], v);
								}
								j++;
							}
						}
						for (Entry<Partition, Integer> e: entries) {
							Partition np = e.getKey().insert(sid);
							if (e.getValue() + weight + LB <= UB) {
							
								Partition nnp = np.merge(TT | (1 << sid));
								Integer val = parent[S].get(nnp);
								if (val == null || val > e.getValue() + weight) {
									parent[S].remove(nnp);
									parent[S].put(nnp, e.getValue() + weight);
									nnp.setPrevL(e.getKey());
									nnp.setPrevS(pS);
									nnp.setE(TT);
									nnp.setPrevNodeL(node.left);
									if (parent[S].size() <= threshold) {
										worst = Math.max(worst, e.getValue() + weight);
									}
								}
							} else {
								break;
							}
						}
						T = (T - 1) & U;
					} while (T != U);
				} else {
					if (g.isTerminal(v)) { // v must be in a solution
						parent[S].clear();
					} else {
						int pS = Bit.remove(S, bid);
						for (Entry<Partition, Integer> e: ch[pS].entrySet()) {
							Partition partition = e.getKey().copy();
							parent[S].put(partition, e.getValue());
							partition.setPrevL(e.getKey());
							partition.setPrevS(pS);
							partition.setPrevNodeL(node.left);
						}
					}
				}
			}
			for (int S = 0; S < ch.length; S++) {
				ch[S].clear();
				ch[S] = null;
			}
		} else if (node.type == Type.forget) {
			HashMap<Partition, Integer>[] ch = dfs(node.left);			
			if (VERBOSE) {
				System.out.println("DP " + completed++ + "/" + nNodes + ", bagsize = " + w + ", node type = forget");
			}
			
			int v = node.vertex(); // forgetting vertex;
			int bid = node.left.indexOf(v);
			
			for (int S = 0; S < 1 << (w + 1); S++) {
				int NS = Bit.remove(S, bid);
				if (Bit.contains(S, bid)) {
					int sid = Bit.index(S, bid);
					for (Entry<Partition, Integer> e: ch[S].entrySet()) {
						if (e.getKey().isSingleton(sid)) {
							continue;
						}
						Partition np = e.getKey().remove(sid);
						Integer val = parent[NS].get(np);
						if (val == null || val > e.getValue()) {
							parent[NS].remove(np);
							parent[NS].put(np, e.getValue());
							np.setPrevL(e.getKey());
							np.setPrevS(S);
							np.setPrevNodeL(node.left);
						}
					}
				} else {
					for (Entry<Partition, Integer> e: ch[S].entrySet()) {
						Integer val = parent[NS].get(e.getKey());
						if (val == null || val > e.getValue()) {
							Partition partition = e.getKey().copy();
							parent[NS].remove(partition);
							parent[NS].put(partition, e.getValue());
							partition.setPrevL(e.getKey());
							partition.setPrevS(S);
							partition.setPrevNodeL(node.left);
						}
					}
				}
			}
			for (int S = 0; S < ch.length; S++) {
				ch[S].clear();
				ch[S] = null;
			}
		} else if (node.type == Type.join) {
			HashMap<Partition, Integer>[] ch1 = dfs(node.left);
			HashMap<Partition, Integer>[] ch2 = dfs(node.right);
			
			if (VERBOSE) {
				System.out.println("DP " + completed++ + "/" + nNodes + ", bagsize = " + w + ", node type = join");
			}
			
			for (int S = 0; S < 1 << w; S++) {
				int worst = -1;
				int LB = lowerbound(node.bag, S, btt);
				Entry<Partition, Integer>[] entries1 = ch1[S].entrySet().toArray(new Entry[0]);
				Entry<Partition, Integer>[] entries2 = ch2[S].entrySet().toArray(new Entry[0]);
				Arrays.sort(entries1, (a, b) -> a.getValue() - b.getValue());
				Arrays.sort(entries2, (a, b) -> a.getValue() - b.getValue());
				for (Entry<Partition, Integer> e1: entries1) {
					for (Entry<Partition, Integer> e2: entries2) {
						int nw = e1.getValue() + e2.getValue();
						if (nw + LB > UB) break;

						Partition np = e1.getKey().join(e2.getKey());
						Integer val = parent[S].get(np);
						if (val == null || val > nw) {
							np.setPrevL(e1.getKey());
							np.setPrevR(e2.getKey());
							np.setPrevS(S);
							np.setPrevNodeL(node.left);
							np.setPrevNodeR(node.right);
							parent[S].remove(np);
							parent[S].put(np, nw);
							if (parent[S].size() <= threshold) {
								worst = Math.min(worst, nw);
							}
						}
					}
				}
			}
			for (int S = 0; S < ch1.length; S++) {
				ch1[S].clear();
				ch2[S].clear();
				ch1[S] = null;
				ch2[S] = null;
			}
		}
		
		if (DEBUG) {
			System.out.println(node + " " + Arrays.toString(node.bag));
			for (int S = 0; S < 1 << w; S++) {
				for (Entry<Partition, Integer> e: parent[S].entrySet()) {
					System.out.println(Integer.toBinaryString(S) + " " + e.getKey() + " " + e.getValue());
				}
			}
		}
		
		int size = 0;
		if (pre_solve) {
			for (int S = 0; S < parent.length; S++) {
				if (parent[S].size() > threshold) {
					Entry<Partition, Integer>[] entries = parent[S].entrySet().toArray(new Entry[0]);
					Arrays.sort(entries, (e1, e2) -> e1.getValue() - e2.getValue());
					parent[S].clear();
					for (int i = 0; i < threshold; i++) {
						parent[S].put(entries[i].getKey(), entries[i].getValue());
						size += parent[S].size();
					}
				}
				parent[S] = reduce(parent[S], Integer.bitCount(S));
			}
		}

		if (!pre_solve) {
			for (int S = 0; S < parent.length; S++) {
				parent[S] = reduce(parent[S], Integer.bitCount(S));
				size += parent[S].size();
			}
		}
		
		return parent;
	}
	
	private int lowerbound(int[] bag, int S, int[][] dist)
	{
		int[] tdist = new int[g.t];
		Arrays.fill(tdist, inf);
		for (int i = 0; i < bag.length; i++) {
			if (Bit.contains(S, i)) {
				for (int j = 0; j < g.t; j++) {
					tdist[j] = Math.min(tdist[j], dist[i][j]);
				}
			}
		}
		int lb = 0;
		for (int i = 0; i < g.t; i++) {
			if (tdist[i] < inf) {
				lb = Math.max(lb, tdist[i]);
			}
		}
		return lb;
	}
	
	private int[][] bagToTerminal(Node node)
	{
		int[][] dist = new int[node.bag.length][g.t];
		XBitSet remainTerms = g.all.subtract(node.decendantBags).intersectWith(terminalSet);
		if (remainTerms.isEmpty()) {
			return dist;
		}
		XBitSet subgraph = g.all.subtract(node.decendantBags).unionWith(node.bagSet);
		for (int i = 0; i < node.bag.length; i++) {
			int[] _dist = distance(node.bag[i], remainTerms, subgraph);
			for (int j = 0; j < g.t; j++) {
				dist[i][j] = _dist[g.terms[j]];
			}
		}
		return dist;
	}

	private int[] distance(int s, XBitSet T, XBitSet subgraph)
	{
		int[] dist = new int[g.n];
		Arrays.fill(dist, inf);
		dist[s] = 0;
		PriorityQueue<int[]> que = new PriorityQueue<>((a, b) -> a[1] - b[1]);
		que.offer(new int[] {s, 0});
		while (que.isEmpty() == false) {
			int[] c = que.poll();
			XBitSet N = g.neighborSet[c[0]].intersectWith(subgraph);
			for (int v = N.nextSetBit(0); v >= 0; v = N.nextSetBit(v + 1)) {
				if (dist[v] > c[1] + g.getEdgeWeight(c[0], v)) {
					dist[v] = c[1] + g.getEdgeWeight(c[0], v);
					que.offer(new int[] {v, dist[v]});
				}
			}
		}
		return dist;
	}

	private HashMap<Partition, Integer> reduce(HashMap<Partition, Integer> map, int N)
	{
		if (N == 0 || map.size() <= 1 << (N - 1)) {
			return map;
		}
		Entry<Partition, Integer>[] entries = map.entrySet().toArray(new Entry[0]);
		Arrays.sort(entries, (a, b) -> a.getValue() - b.getValue());
		BitSet[] matrix = new BitSet[entries.length];
		int[] position = new int[matrix.length];
		for (int i = 0; i < entries.length; i++) {
			matrix[i] = new BitSet(1 << (N - 1));
			position[i] = i;
			for (int S = 0; S < 1 << (N - 1); S++) {
				int S_0 = (S << 1) + 1;         // with 0
				int S_1 = ((1 << N) - 1) ^ S_0; // without 0
				if (entries[i].getKey().isRefinement(S_0, S_1)) {
					matrix[i].set(S);
				}
			}
		}
		
		HashMap<Partition, Integer> result = new HashMap<>();
		
		for (int r = 0, c = 0; r < matrix.length && c < 1 << (N - 1); c++) {
			int pivot = -1;
			for (int i = r; i < matrix.length; i++) {
				if (matrix[i].get(c)) {
					pivot = i;
					break;
				}
			}
			if (pivot >= 0) {
				result.put(entries[position[pivot]].getKey(), entries[position[pivot]].getValue());
				BitSet tmp = matrix[pivot];
				int tmppos = position[pivot];
				for (int i = pivot; i > r; i--) {
					matrix[i] = matrix[i - 1];
					position[i] = position[i - 1];
				}
				matrix[r] = tmp;
				position[r] = tmppos;
				BitSet row = matrix[r];
				for (int i = r + 1; i < matrix.length; i++) {
					if (matrix[i].get(c)) {
						matrix[i].xor(row);
					}
				}
				
				r++;
			}
		}
		return result;
	}
	
	public static void run()
	{
		Object[] gtd = WeightedGraph.readGraphGrwithTd();
		WeightedGraph g = (WeightedGraph)gtd[0];
		TreeDecomposition td = (TreeDecomposition)gtd[1];
		Preprocessing pre = new Preprocessing();
		int ans = 0;
		HashSet<int[]> edges = new HashSet<>();
//		Pair<WeightedGraph, TreeDecomposition> contracted = pre.contractMaximalChains(g, td);
		ArrayList<Pair<WeightedGraph, TreeDecomposition>> comps = pre.reduce(g, td);
		for (Pair<WeightedGraph, TreeDecomposition> comp: comps) {
			if (comp.second.width >= 16) {
				DP dp = new DP(comp.first);
				Pair<Integer, TreeSet<int[]>> pre_sol = dp.solve();
				ans += pre_sol.first;
				edges.addAll(pre_sol.second);
			} else {
				NiceTreeDecomposition nntd = new NiceTreeDecomposition(comp.first, comp.second);
				TreewidthDP tdp = new TreewidthDP(comp.first, nntd);
				Pair<Integer, HashSet<int[]>>pre_sol = tdp.solve();
				ans += pre_sol.first;
				edges.addAll(pre_sol.second);
			}
		}
		System.out.println("VALUE " + ans);
		for (int[] edge: edges) {
			System.out.println(edge[0] + " " + edge[1]);
		}
	}

	
//	private static final boolean DEBUG = true;
	private static final boolean DEBUG = false;
	
//	private static final boolean VERBOSE = true;
	private static final boolean VERBOSE = false;
	
	private boolean pre_solve = true;

	private int threshold = 1 << 6;
}
