#!/bin/sh

rm -r bin
mkdir bin
javac -Xlint:none -d bin ./src/*.java ./src/algo/*.java ./src/tw/exact/*.java ./src/graph/*.java ./src/util/*.java
